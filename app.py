###############################################################################
# VAULT401                                                                    #
# by Carles Hernandez-Ferrer                                                  #
# (C) Boston Children's Hospital                                              #
###############################################################################

# IMPORTS
###############################################################################
import random
import string
import datetime
from sys import stderr as err
from os import environ as env
from os import path as pth
from flask import Flask, Blueprint, render_template, redirect, url_for, request, flash, send_file
from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin, LoginManager, login_user, login_required, logout_user, current_user

# FUNCTIONS
###############################################################################

# Create a random string
def generate_random_string():
    return ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(10)])

# Create tag for file
def generate_tag(user_id):
    now = datetime.datetime.now()
    return 'x.' + str(user_id) + '.' + str(now.year) + '.' + generate_random_string() + '.' + str(now.month) + '.' + str(now.day) + '.' + generate_random_string()

# Add a string (comment) to apache2 error log
def write_log(txt):
    err.write('[FOLDER LOG]: %s\n' % str(txt))
    err.flush()
    
# Function to validate a given path to a file
def validate_path(path):
    x = pth.isfile(path)
    write_log('Validating file "' + path + '" --> ' + str(x))
    return x

# DATABASE
###############################################################################

# Initialization of SQLAlchemy so we can use it our models
db = SQLAlchemy()

# DDBB models: user
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(150))
    surname = db.Column(db.String(300))
    institution = db.Column(db.String(500))
    email = db.Column(db.String(500))
    password = db.Column(db.String(50))
    type = db.Column(db.String(25))
    enable = db.Column(db.Boolean(), default = True)

    def __str__(self):
        return'[USER]: ' + self.name + '(id: ' + str(self.id) + '; enable: ' + str(self.enable) + ')'

# DDBB models: file
class File(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    idSender = db.Column(db.Integer, db.ForeignKey('user.id'), nullable = False)
    file_path = db.Column(db.String(5000))
    display_name = db.Column(db.String(100))
    shareable = db.Column(db.Boolean(), default = True)

    def __str__(self):
        return'[FILE]: ' + self.display_name + '(id: ' + str(self.id) + '; owner: ' + str(self.idSender) + ')'

# DDBB models: access
class Access(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    idFile = db.Column(db.Integer)
    idReceiver = db.Column(db.Integer)
    created_at = db.Column(db.DateTime)
    valid_until = db.Column(db.DateTime)
    internal_link = db.Column(db.String(5000))
    available_down = db.Column(db.Integer)
    used_down = db.Column(db.Integer)

    def __str__(self):
        return'[ACCESS]: (id: ' + str(self.id) + '; file: ' + str(self.idFile) + '; receiver: ' + str(self.idReceiver) + ')'


# MAIN APPLICATION ENDPOINTS
###############################################################################
# They include:
#    - / and /home: to show the main page (index.html)
#    - /profile: to show user's profile and update password
#    - /admin: to show administrator menu
#    - /add_user: to allow administrator to add a new user
#    - /manage_user: to allow administrator to manage existing users
#    - /add_file: to allow administrator to add new files to share
#    - /manage_file: to allow administrator to manage existing files
#    - /add_access: to allow administrator to grant access to existing files
#    - /manage_access: to allow administrator to see granted accesses


main = Blueprint('main', __name__)

# -- INDEX ------------------------------------------------------------------
@main.route('/')
def index():
    return render_template('index.html')

@main.route('/home')
def home():
    return render_template('index.html')

# -- PROFILE ------------------------------------------------------------------
@main.route('/profile')
@login_required
def profile():
    write_log('Profile for "' + current_user.name + '"')
    return render_template('profile.html', name = current_user.name,\
        surname = current_user.surname, institution = current_user.institution,\
        email = current_user.email, enable = current_user.enable)

@main.route('/profile', methods=['POST'])
@login_required
def profile_post():
    write_log('User asked to update the password')
    password_n1 = request.form.get('password_n1')
    password_n2 = request.form.get('password_n2')
    if password_n1 != password_n2:
        write_log('Update status: given passwords do not match')
        flash('Your password and confirmation password do not match.')
        return redirect(url_for('main.profile'))
    return redirect(url_for('main.profile'))

# -- ADMIN - MENU -------------------------------------------------------------
@main.route('/admin')
@login_required
def admin():
    return render_template('admin.html')

# -- ADMIN - ADD NEW USER -----------------------------------------------------
@main.route('/add_user')
@login_required
def add_user():
    if current_user.type == 'Administrator':
        return render_template('add_user.html', password = generate_random_string())
    else:
        return redirect(url_for('main.profile'))

@main.route('/add_user', methods=['POST'])
@login_required
def add_user_post():
    write_log('Admin "' + current_user.name + '" asked to create a new user')
    name = request.form.get('name').strip()
    surname = request.form.get('surname').strip()
    institution = request.form.get('institution').strip()
    password = request.form.get('password')
    email_n1 = request.form.get('email_m1').strip()
    email_n2 = request.form.get('email_m2').strip()
    type = request.form.get('type')
    if email_n1 != email_n2:
        write_log('Add status: given emails do not match')
        flash('Provided email and confirmation do not match.')
        return redirect(url_for('main.add_user'))
    elif email_n1 == '' or email_n2 == '':
        write_log('Add status: one of the e-mails is empty')
        flash('Provided email cannot be empty.')
        return redirect(url_for('main.add_user'))
    elif name == '' or surname == '' or institution == '':
        write_log('Add status: name, surname, or institution is empty')
        flash('Name, surname, and institution cannot be empty.')
        return redirect(url_for('main.add_user'))
    else:
        # information seems correct
        user = User.query.filter_by(email = email_n1).first()
        if user: # existing user
            write_log('Add status: email already in the databse')
            flash('The provided email is already assigned to a user.')
            return redirect(url_for('main.add_user'))
        else: # generate_password_hash(password, method='sha256')
            new_user = User(name = name, surname = surname, institution = institution, email = email_n1, password = password, type = type, enable = True)
            # add the new user to the database
            db.session.add(new_user)
            db.session.commit()
    return redirect(url_for('main.admin'))

# -- ADMIN - MANAGE USERS -----------------------------------------------------
@main.route('/manage_user')
@login_required
def manage_user():
    if current_user.type == 'Administrator':
        users = User.query.all()
        return render_template('manage_user.html', items = users)
    else:
        return redirect(url_for('main.profile'))

# -- ADMIN - ADD NEW FILE -----------------------------------------------------
@main.route('/add_file')
@login_required
def add_file():
    if current_user.type == 'Administrator':
        return render_template('add_file.html', user_id = current_user.id, user_name = current_user.name)
    else:
        return redirect(url_for('main.profile'))

@main.route('/add_file', methods=['POST'])
@login_required
def add_file_post():
    write_log('Admin "' + current_user.name + '" asked to create a new file')
    user_id = int(request.form.get('owner_id').strip())
    display_file = request.form.get('display').strip()
    path_file = request.form.get('path').strip()
    if display_file == '':
        write_log('Add status: display name is empty')
        flash('Every new file must have a display name.')
        return redirect(url_for('main.add_file'))
    elif path_file == '':
        write_log('Add status: path is empty')
        flash('A path to a file is required.')
        return redirect(url_for('main.add_file'))
    elif not validate_path(path_file):
        write_log('Add status: path not valid')
        flash('Provided path is invalid.')
        return redirect(url_for('main.add_file'))
    else:
        new_file = File(idSender = user_id, file_path = path_file, display_name = display_file, shareable = True)
        # add the new file to the database
        db.session.add(new_file)
        db.session.commit()
        return redirect(url_for('main.admin'))

# -- ADMIN - MANAGE FILE ------------------------------------------------------
@main.route('/manage_file')
@login_required
def manage_file():
    if current_user.type == 'Administrator':
        files = File.query.all()
        return render_template('manage_file.html', items=files)
    else:
        return redirect(url_for('main.profile'))

# -- ADMIN - ADD ACCESS -------------------------------------------------------
@main.route('/add_access')
@login_required
def add_access():
    if current_user.type == 'Administrator':
        files = File.query.filter_by(shareable = True)
        users = User.query.filter_by(enable  =True).filter_by(type = 'User')
        return render_template('add_access.html', files = files, users = users)
    else:
        return redirect(url_for('main.profile'))

@main.route('/add_access', methods=['POST'])
@login_required
def add_access_post():
    write_log('Admin "' + current_user.name + '" asked to create a new access')
    receiver_id = int(request.form.get('user_id').strip())
    file_id = request.form.get('file_id').strip()
    ava_from = request.form.get('date_from').strip()
    ava_until = request.form.get('date_until').strip()
    cnt = request.form.get('allowed_downloads').strip()
    try:
        cnt = int(cnt)
        if cnt < 1:
            cnt = 5
    except e:
        cnt = 5

    if ava_from == '' or ava_until == '':
        write_log('Add status: one of the dates is empty')
        flash('Incorrect dates.')
        return redirect(url_for('main.add_access'))

    acc = Access(idFile = file_id, idReceiver = receiver_id, created_at = ava_from, valid_until = ava_until, internal_link = generate_tag(receiver_id), available_down = cnt, used_down = 0)
    write_log(str(acc))
    db.session.add(acc)
    db.session.commit()
    return redirect(url_for('main.admin'))

# -- ADMIN - MANAGE ACCESS ----------------------------------------------------
@main.route('/manage_access')
@login_required
def manage_access():
    if current_user.type == 'Administrator':
        acc = db.session.query(User, File, Access)\
            .filter(User.id == Access.idReceiver)\
            .filter(File.id == Access.idFile)\
            .all()
        return render_template('manage_access.html', items=acc)
    else:
        return redirect(url_for('main.profile'))

# -- USER - LIST FILES --------------------------------------------------------
@main.route('/files')
@login_required
def files():
    if current_user.type == 'User':
        acc = db.session.query(File, Access)\
            .filter(Access.idReceiver == current_user.id)\
            .filter(File.id == Access.idFile)
        return render_template('files.html', items = acc, bname=pth.basename)
    else:
        return redirect(url_for('main.profile'))

# AUTHENTICATION APPLICATION ENDPOINTS
###############################################################################
# They include:
#    - /login
#    - /logout

auth = Blueprint('auth', __name__)

# -- LOGIN --------------------------------------------------------------------
@auth.route('/login')
def login():
    return render_template('login.html')

@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    write_log('Accessed loggin [POST] for "' + email + '"')
    user = User.query.filter_by(email=email).first()
    # check if user actually exists
    # take the user supplied password, hash it, and compare it to the hashed password in database
    if not user or not user.password == password: #check_password_hash(user.password, password):
        write_log('Authentication results: not in database')
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login')) # if user doesn't exist or password is wrong, reload the page
    else:
        write_log('Autherntication results: in database with id "' + str(user.id) + '"')
        # if the above check passes, then we know the user has the right credentials
        login_user(user, remember = False)
        if user.type == 'User':
            return redirect(url_for('main.profile'))
        elif user.type == 'Administrator':
            return redirect(url_for('main.admin'))
        else:
            return redirect(url_for('main.home'))

# -- LOGOUT -------------------------------------------------------------------
@auth.route('/logout')
@login_required
def logout():
    write_log('Access logout for "' + current_user.email + '"')
    logout_user()
    return redirect(url_for('main.index'))

# API APPLICATION ENDPOINTS
###############################################################################
# They include:
#    - /file

api = Blueprint('api', __name__)

@api.route('/file', methods=['GET'])
@api.route('/file/<tag>', methods=['GET'])
def file(tag):
    write_log('[API] requested tag "' + str(tag) + '"')
    acc = Access.query.filter_by(internal_link = tag).first()

    now = datetime.datetime.now()
    if now > acc.valid_until:
        write_log('Token "' + str(acc) + '" expired.')
        return 'The token has expired.', 409
    elif acc.used_down >= acc.available_down:
        write_log('[API] Reached maximum available downloads.')
        return 'Reached maximum available downloads.', 409
    else:
        acc.used_down += 1
        db.session.commit()
        acc = db.session.query(File, Access)\
            .filter(Access.internal_link == tag)\
            .filter(File.id == Access.idFile).first()
        write_log('[API] Status correct, proceed to send "' + pth.basename(acc.File.file_path) + '"')
        #return '', 200
        return send_file(acc.File.file_path, attachment_filename=pth.basename(acc.File.file_path), as_attachment=True)


# CREATING FLASK APPLICATION
###############################################################################

# Create and register the app
app = Flask(__name__)
app.config['SECRET_KEY'] = env['APP_SECRET']
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://' + env['DDBB_USER'] + ':' + env['DDBB_PASSWORD'] + '@' + env['DDBB_HOST'] + ':' + env['DDBB_PORT'] + '/' + env['DDBB_NAME']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.register_blueprint(auth)
app.register_blueprint(main)
app.register_blueprint(api)

db.init_app(app)

# setting up the loggin manager
login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.init_app(app)


# Set-up validation
@login_manager.user_loader
def load_user(user_id):
    # since the user_id is just the primary key of our user table, use it in the query for the user
    return User.query.get(int(user_id))

if __name__ == '__main__':
    app.run()
